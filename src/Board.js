import React, {useEffect, useRef, useState, useLayoutEffect} from 'react';
import styles from './Board.module.css';
import PropTypes from 'prop-types';
import logoImage from './logo.svg';

function Board(props) {
    const {maze, currentCell, timer, time} = props;
    const canvas = useRef(null);
    const container = useRef(null);
    const [ctx, setCtx] = useState(undefined);
    const [goalOpacity, setGoalOpacity] = useState(1);
    const [blockWidth, setBlockWidth] = useState(0);
    const [blockHeight, setBlockHeight] = useState(0);

    useEffect(() => {
        const fitToContainer = () => {
            const {offsetWidth, offsetHeight} = container.current;
            canvas.current.width = offsetWidth;
            canvas.current.height = offsetHeight;
            canvas.current.style.width = offsetWidth + 'px';
            canvas.current.style.height = offsetHeight + 'px';
        };

        setCtx(canvas.current.getContext('2d'));
        setTimeout(fitToContainer, 0);
    }, []);

    useEffect(() => {
        setGoalOpacity(goalOpacity === 1 ? 0 : 1)

        // if(maze !== undefined){
        //     const col = Math.floor(Math.random()*100 % maze.cols);
        //     const row = Math.floor(Math.random()*100 % maze.rows);
        //
        //     const x = col * blockWidth
        //     const y = row * blockHeight
        //     const cell = {x:col,y:row}
        //
        //     if(timer === LOLLYPOP_APPEARING_TIME){
        //         setPrizes([...prizes, {point:{x,y},img:lollipopImage, cell}])
        //     }
        //     if(timer === ICECREAM_APPEARING_TIME){
        //         setPrizes([...prizes, {point:{x,y},img:iceCreamImage, cell}])
        //     }
        // }
    },[timer])

    const _drawImage = ({deltaX=1,deltaY=1,blockWidth,blockHeight,xOffset=0,yOffset=0,imgSrc}) => {
        const logoSize = 0.75 * Math.min(blockWidth, blockHeight);
        const image = new Image(logoSize, logoSize);
        image.onload = () => {
            ctx.drawImage(image, deltaX * blockWidth + xOffset + (blockWidth - logoSize) / 2, deltaY * blockHeight + yOffset + (blockHeight - logoSize) / 2, logoSize, logoSize);
        };

        image.src = imgSrc;
    }

    useEffect(() => {
        const drawLine = (x1, y1, width, height) => {
            ctx.strokeStyle = 'white';
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x1 + width, y1 + height);
            ctx.stroke();
        };

        const draw = () => {
            if (!maze) {
                return;
            }

            setBlockWidth(Math.floor(canvas.current.width / maze.cols));
            setBlockHeight(Math.floor(canvas.current.height / maze.rows));

            ctx.fillStyle = 'blue';
            ctx.fillRect(0, 0, canvas.current.width, canvas.current.height);

            const xOffset = Math.floor((canvas.current.width - maze.cols * blockWidth) / 2);

            for (let y = 0; y < maze.rows; y++) {
                for (let x = 0; x < maze.cols; x++) {
                    const cell = maze.cells[x + y * maze.cols];
                    const walls = cell.walls;
                    if (y === 0 && walls[0]) {
                        drawLine(blockWidth * x + xOffset, blockHeight * y, blockWidth, 0)
                    }
                    if (walls[1]) {
                        drawLine(blockWidth * (x + 1) + xOffset, blockHeight * y, 0, blockHeight);
                    }
                    if (walls[2]) {
                        drawLine(blockWidth * x + xOffset, blockHeight * (y + 1), blockWidth, 0);
                    }
                    if (x === 0 && walls[3]) {
                        drawLine(blockWidth * x + xOffset, blockHeight * y, 0, blockHeight);
                    }

                    if(cell.prizes.length > 0){
                        cell.prizes.forEach(p => {
                            if(p.timeToAppear > 0 && timer >= p.timeToAppear && !p.taken)
                                _drawImage({blockWidth, blockHeight, imgSrc:p.img, xOffset:(x-1)*blockWidth, yOffset:(y-1)*blockHeight})
                            else if(p.timeToAppear < 0 && time + p.timeToAppear <= 0 && !p.taken)
                                _drawImage({blockWidth, blockHeight, imgSrc:p.img, xOffset:(x-1)*blockWidth, yOffset:(y-1)*blockHeight})
                        })
                    }
                }
            }

            _drawImage({deltaX:currentCell[0],deltaY:currentCell[1],blockWidth,blockHeight,xOffset,imgSrc:logoImage})

            const textSize = Math.min(blockWidth, blockHeight);
            ctx.fillStyle = "rgba(255, 0, 0, " + goalOpacity + ")";
            ctx.font = '20px "Joystix"';
            ctx.textBaseline = 'top';
            ctx.fillText('Goal', maze.endCell[1] * blockWidth + xOffset + (blockWidth - textSize) / 2, maze.endCell[0] * blockHeight + (blockHeight - textSize) / 2, textSize)
        };

        draw();
    }, [ctx, ...currentCell, maze, timer, goalOpacity]);

    return (
        <div
            className={styles.root}
            ref={container}
        >
            <canvas ref={canvas}/>
        </div>
    );
}

Board.propTypes = {
    maze: PropTypes.shape({
        cols: PropTypes.number.isRequired,
        rows: PropTypes.number.isRequired,
        cells: PropTypes.array.isRequired,
        currentCell: PropTypes.arrayOf(PropTypes.number)
    })
};

export default Board;
