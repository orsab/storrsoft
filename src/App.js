import React, {useCallback, useState, useEffect, useReducer} from 'react';
import styles from './App.module.css';
import useInterval from "@use-it/interval";
import Header from './Header';
import Notification from './Notification';
import MazeGenerator from './maze/MazeGenerator';
import Board from './Board';
import mazeSound from './audio/maze.mp3';
import finishSound from './audio/level_end.mp3';

export const ROUND_TIME = 60;
const ROWS = 17;
const COLS = 33;

export const LOLLYPOP_APPEARING_TIME = 30;
export const ICECREAM_APPEARING_TIME = -15;

function reducer(state, action) {
    switch (action.type) {
        case 'startGame': {
            return {
                ...state,
                gameEnded:false,
                round: action.payload.incRound ? state.round + 1 : state.round,
                sound: {src:mazeSound,loop:true},
                maze: action.payload.maze,
                currentCell: action.payload.maze.startCell,
                time: ROUND_TIME
            };
        }
        case 'decrementTime': {
            return {
                ...state,
                time: state.time - 1
            };
        }
        case 'move': {
            if(state.gameEnded)
                return state;

            const {direction} = action.payload
            const testCell = [...state.currentCell]
            switch(direction){
                case 'up': testCell[1]--; break;
                case 'down': testCell[1]++; break;
                case 'left': testCell[0]--; break;
                case 'right': testCell[0]++; break;
            }

            const canMove = state.maze.canMove(direction, testCell[0], testCell[1])

            if(canMove){
                if(testCell[0] === COLS-1 && testCell[1] === ROWS-1){
                    return {
                        ...state,
                        points: state.points + (state.round * 100 * state.time),
                        currentCell: testCell,
                        gameEnded:true,
                        sound:{src:finishSound, onended:action.payload.onended},
                        time:undefined,
                        hiScore: Math.max(state.hiScore, state.points)
                    };
                }
                return {
                    ...state,
                    points: state.points + 10,
                    currentCell: testCell
                };
            }

            return state;
        }
        case 'endGame': {
            return {
                ...state,
                gameEnded:true,
                sound:undefined,
                hiScore: Math.max(state.hiScore, state.points)
            }
        }
        case 'prizeTaken': {
            const cell = state.maze.cells[state.currentCell[0] + state.currentCell[1] * state.maze.cols]
            cell.prizes = []

            return {
                ...state,
                points: state.points + action.payload.bonusPoints,
                time: state.time + action.payload.bonusTime
            }
        }
        default:
            throw new Error("Unknown action");
    }
}

function App() {
    const [state, dispatch] = useReducer(reducer, {
        points: 0,
        round: 1,
        hiScore: 0,
        time: undefined,
        maze: undefined,
        sound:undefined,
        gameEnded: false,
        currentCell: [0,0]
    });

    const [player, setPlayer] = useState(null)
    const [timer, setTimer] = useState(0);
    const handleOnEnterKeyPressed = useCallback(() => {
        if (!state.time) {
            _onEnded(false)
        }
    }, [state.time]);

    const _onEnded = (incRound=true) => {
        dispatch({
            type: 'startGame',
            payload: {
                incRound,
                maze: new MazeGenerator(ROWS, COLS).generate()
            }
        })
    };

    useEffect(() => {
        const onKeyDown = e => {
            switch(e.keyCode){
                case 13: handleOnEnterKeyPressed(); break;
                case 38: dispatch({type:'move',payload:{direction:'up',onended:_onEnded}}); break;
                case 40: dispatch({type:'move',payload:{direction:'down',onended:_onEnded}}); break;
                case 37: dispatch({type:'move',payload:{direction:'left',onended:_onEnded}}); break;
                case 39: dispatch({type:'move',payload:{direction:'right',onended:_onEnded}}); break;
            }
        };
        window.addEventListener('keydown', onKeyDown);

        return () => {
            window.removeEventListener('keydown', onKeyDown);
        }
    }, [handleOnEnterKeyPressed]);

    useInterval(() => {
        dispatch({type: 'decrementTime'})
    }, state.time ? 1000 : null);

    useInterval(()=>{
        setTimer(timer+1)
    }, state.time ? 1000 : null)

    useEffect(() => {
        if (state.time === 0) {
            dispatch({type: 'endGame'});
        }
    }, [state.time]);

    useEffect(() => {
        if(!state.maze)
            return

        let prizes = state.maze.getPrize(state.currentCell[0], state.currentCell[1])
        if(prizes.length > 0){
            prizes.forEach(p => {
                if(p.timeToAppear >= 0 && timer >= p.timeToAppear || p.timeToAppear < 0 && state.time + p.timeToAppear <= 0){
                    dispatch({type:'prizeTaken', payload:p})
                }
            })
        }
    }, [state.currentCell]);

    useEffect(() => {
        if(player){
            player.pause();
            player.remove();
            player.currentTime = 0;
        }
        else{
            setPlayer(new Audio());
        }

        if(state.sound){
            player.src = state.sound.src;
            player.loop = !!state.sound.loop;

            if(state.sound.onended){
                player.onended = state.sound.onended
            }

            setTimeout(()=>player.play(),0)
        }
    }, [state.sound]);

    return (
        <div className={styles.root}>
            <Header
                hiScore={state.hiScore}
                points={state.points}
                time={state.time}
                round={state.round}
            />
            <Board
                maze={state.maze}
                time={state.time}
                timer={timer}
                currentCell={state.currentCell}
            />
            <Notification
                show={!state.time}
                gameOver={state.time === 0}
            />
        </div>
    );
}

export default App;
