class Prize {
    constructor({name, timeToAppear, bonusPoints, bonusTime,img}) {
        this.name = name;
        this.timeToAppear = timeToAppear;
        this.bonusPoints = bonusPoints;
        this.bonusTime = bonusTime;
        this.img = img;
        this.taken = false;
    }

}

export default Prize;