import Prize from "./Prize";

class Maze {
    constructor(rows, cols, cells, startCell, endCell) {
        this.rows = rows;
        this.cols = cols;
        this.cells = cells;
        this.startCell = startCell;
        this.endCell = endCell;
    }

    canMove(direction, x, y){
        const cell = this.cells[x + y * this.cols]

        if(!cell)
            return false;

        const walls = cell.walls;

        const [down, left, up, right] = walls;
        const check = {left,right,up,down};

        return !(check[direction]);
    }

    getPrize(x, y){
        const cell = this.cells[x + y * this.cols]

        return cell.prizes;
    }
}

export default Maze;